# NamesAcadTable
AutoCAD add-in -
 insert names into block for particular industry

## Commands

|Command | Command Description |
 |---|---|
 | FillTablesWithIndustryNames |  it  will execute procedures to insert fixed names into all blocks in the AutoCAD . User can select industry that correspond names to block. The block is a table with attributes |

## How to autoload DLLs with AutoCAD & How it works
 you must load dll file from this repository. You can find this in the released page
 ***NamesAcadTable.dll***

 Open AutoCAD and use ***APPLOAD*** function to add it in the autoload case

## Developing

In order to build the solution you need to add reference to the projects. Most common dlls you would reference are accoremgd.dll, acdbmgd.dll and acmgd.dll, but there are others if you need more modifications. These libraries allow you to manipulate native AutoCAD objects though a .NET language.

You can find these files in ***"ObjectARX SDK 2019"***. You must download this from Autodesk website.

I used version 2019 but you need different version if you use others version of the AutoCAD.

Also for this version of the app you need
***.NET Framework Version 4.7***
(This is due to references from  ObjectARX SDK 2019)

## Why this plugin?
I have more than 50 tables in the AutoCAD files. I want to fill this tables as fast as possible. This plugin do it for me. I use only one command.
