﻿using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.EditorInput;

[assembly: ExtensionApplication(typeof(NamesAcadTable.AcadCommand))]

namespace NamesAcadTable
{

    public enum IndustryEnum
    {
        ELECTRICAL,
        SANITARY,
        ROAD
    }

    public class AcadCommand : IExtensionApplication
    {
        public void Initialize()
        {
            //throw new NotImplementedException();
        }

        public void Terminate()
        {
            //throw new NotImplementedException();
        }

        [CommandMethod("FillTablesWithIndustryNames")]
        public void FillTablesWithIndustryNames()
        {
            Document doc = Application.DocumentManager.MdiActiveDocument;
            Editor ed = doc.Editor;

            //Prompts for each industry
            PromptStringOptions prOpNum = new PromptStringOptions("Choose industry type Electrical/Sanitary/Road<Electrical>: ")
            {
                AllowSpaces = true,
                DefaultValue = IndustryEnum.ELECTRICAL.ToString(),
                UseDefaultValue = true
            };

            //Add keywords when prompting for position
            prOpNum.Keywords.Add(IndustryEnum.ELECTRICAL.ToString());
            prOpNum.Keywords.Add(IndustryEnum.SANITARY.ToString());
            prOpNum.Keywords.Add(IndustryEnum.ROAD.ToString());


            PromptResult industryType = doc.Editor.GetString(prOpNum);

            ///manualy initialize local member
            IndustryName indType = null;

            var selectedIndustry = industryType.StringResult.ToUpper();

            try
            {
                if (selectedIndustry == IndustryEnum.ELECTRICAL.ToString())
                {
                    indType = new Electrical().IndustryCreate();
                }

                if (selectedIndustry == IndustryEnum.SANITARY.ToString())
                {
                    indType = new Sanitary().IndustryCreate();
                }

                if (selectedIndustry == IndustryEnum.ROAD.ToString())
                {
                    indType = new Road().IndustryCreate();
                }

                //edit block atributes
                //updtade block with hardcoded id value
                BlockAttributes.Use(blkAtt => { blkAtt.UpdateAcadBlockAttributes("A$C70146EFE", indType); });

            }
            catch (Exception)
            {
                ed.WriteMessage("exception error or user canceled");
            }
        }
    }
}
