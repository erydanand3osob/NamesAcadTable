﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NamesAcadTable
{
    public abstract class IndustryNameTypes
    {
        protected string DesignedBy;
        protected string DevelopedBy;
        protected string VerifiedBy;
        protected string GetIndustryName;

        protected IndustryNameTypes AddDesigner(string designer)
        {
            DesignedBy = designer;
            return this;
        }
        protected IndustryNameTypes AddDeveloper(string developer)
        {
            DevelopedBy = developer;
            return this;
        }
        protected IndustryNameTypes AddVerifier(string verification)
        {
            VerifiedBy = verification;
            return this;
        }
        protected IndustryNameTypes AddIndustryName(string name)
        {
            GetIndustryName = name;
            return this;
        }

        public IndustryName IndustryCreate()
        {
            return new IndustryName(DesignedBy, DevelopedBy, VerifiedBy, GetIndustryName);
        }
    }
    public class Electrical : IndustryNameTypes
    {
        public Electrical()
        {
            AddDesigner("Jan Kowalski");
            AddDeveloper("Janina Kowalska");
            AddVerifier("Teodor Przykładowy");
            AddIndustryName("Branża elektryczna");
        }
    }

    public class Sanitary : IndustryNameTypes
    {
        public Sanitary()
        {
            AddDesigner("Jan Rura");
            AddDeveloper("Pola Kowalska");
            AddVerifier("Lilia Kowalska");
            AddIndustryName("Branża sanitarna");
        }
    }

    public class Road : IndustryNameTypes
    {
        public Road()
        {
            AddDesigner("Jack Roads");
            AddDeveloper("Katrine Tree");
            AddVerifier("Bruce Su");
            AddIndustryName("Branża drogowa");
        }
    }
}
